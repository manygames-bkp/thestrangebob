﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampController : MonoBehaviour, ISwitchable
{ 
    public Material lampOnMat;
    public Material lampOffMat;
    private bool isOff = true;

    public void OnSwitch()
    {
        isOff = !isOff;
        ChangeLampState();
    }

    private void ChangeLampState()
    {
        if (!isOff)
        {
            GetComponent<Light>().intensity = 2000;
            GetComponent<SpriteRenderer>().material = lampOnMat;
        }
        else
        {
            GetComponent<Light>().intensity = 1;
            GetComponent<SpriteRenderer>().material = lampOffMat;
        }
    }
}
