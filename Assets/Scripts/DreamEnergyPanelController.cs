﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class DreamEnergyPanelController : MonoBehaviour, IDialogueOnTouch
    {
        public AudioClip endingSound;
        public AudioClip duringSound;

        public String[] messagesWhenFull = { "Bob...what the hell is going on here?", "Wait wait wait... i know", "this thing is imprisoning your dreams", "but i think i can take it back, just a second" };
        public String[] messagesWhenEmpty = { "It looks so... monochromatic now" };

        private SpriteRenderer energyPanelRenderer;
        private bool isFull = true;
        private Companion companion;

        // Use this for initialization
        void Start()
        {
            energyPanelRenderer = this.GetComponent<SpriteRenderer>();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                OnTouch(other);
            }
        }

        public void OnTouch(Collider2D other)
        {
            if (other.tag == "Player")
            {
                companion = other.GetComponentInChildren<Companion>();
                // We are not filled with energy yet so: funny message, no callback and no freeze
                if (companion.IsFull() && !isFull)
                {
                    companion.Talk(messagesWhenEmpty);
                    return;
                }
                other.GetComponent<Character>().MovementState.ChangeState(CharacterStates.MovementStates.Idle);
                LevelManager.Instance.FreezeCharacters();
                companion.Talk(messagesWhenFull, this);
            }
        }

        public void CallBackAction()
        {
            if (isFull && !companion.IsFull())
            {
                isFull = false;
                foreach (Transform child in transform)
                {
                    if (child.name == "FullEnergyCenter")
                    {
                        StartCoroutine(DisableSprite(child));
                    }
                }
            }
        }

        private IEnumerator DisableSprite(Transform child)
        {
            float delay = 0.5f;
            foreach (Transform nephew in child)
            {
                delay -= (child.childCount * 0.0015f);
                nephew.GetComponent<SpriteRenderer>().enabled = false;
                PlaySoundDuring();
                yield return new WaitForSeconds(delay);
            }
            PlaySoundEnding();
            companion.ReceiveEnergyFrom(this.gameObject);
            LevelManager.Instance.UnFreezeCharacters();
        }

        private void PlaySoundEnding()
        {
            PlaySound(endingSound);
        }

        private void PlaySoundDuring()
        {
            PlaySound(duringSound);
        }

        private void PlaySound(AudioClip sound)
        {
            if (sound != null)
                SoundManager.Instance.PlaySound(sound, transform.position);
        }


    }
}
