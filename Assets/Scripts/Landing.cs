﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class Landing : MonoBehaviour {

    public Animator bobAnimator;
    private InputManager manager;
    private MMTouchButton touchButtonA;
    private MMTouchButton touchButtonRT;

    //private Animator swordEffectAnimator;

    //public GameObject swordEffectPrefab;
    //private GameObject swordEffect;

    //bool facingRight;
    //bool swordEffectRight = false;

    void Awake()
    {
        
    }

    void Start()
    {
        manager = GameObject.Find("UICamera").GetComponent<InputManager>();
        #if UNITY_ANDROID
            touchButtonA = GameObject.FindGameObjectWithTag("ButtonA").GetComponent<MMTouchButton>();
            touchButtonRT = GameObject.FindGameObjectWithTag("ButtonRT").GetComponent<MMTouchButton>();
        #endif
        //manager = GameObject.Find("UICamera").GetComponent<InputManager>();
        //swordEffect = Instantiate(swordEffectPrefab, bobAnimator.rootPosition, bobAnimator.transform.rotation) as GameObject;
        //swordEffect.GetComponent<SpriteRenderer>().enabled = true;
        //swordEffectAnimator = swordEffect.GetComponent<Animator>();
        //swordAnim.Play("SwordEffect");
    }

    //private void Update()
    //{
    //swordEffect.transform.position = bobAnimator.rootPosition + new Vector3(0.15f, -0.02f, 0);
    //}

    public void LandingBegin()
    {
        bobAnimator.SetBool("Landing", true);
        manager.InputDetectionActive = false;
        #if UNITY_ANDROID
            touchButtonA.DisableButton();
            touchButtonRT.DisableButton();
        #endif
    }

    public void LandingEnd()
    {
        bobAnimator.SetBool("Landing", false);
        manager.InputDetectionActive = true;
        #if UNITY_ANDROID
            touchButtonA.EnableButton();
            touchButtonRT.EnableButton();
        #endif

    }

    //public void SwordEffect()
    //{
        //swordEffectAnimator.SetTrigger("SwordEffect");
    //}
}
