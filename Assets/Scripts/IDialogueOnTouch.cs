﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDialogueOnTouch {
    void OnTouch(Collider2D other);
    void CallBackAction();
}
