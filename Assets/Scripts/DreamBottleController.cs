﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;
namespace MoreMountains.CorgiEngine {

    public class DreamBottleController : MonoBehaviour, IDialogueOnTouch {

        public Sprite fullSprite;
        public AudioClip sound;

        public String[] messagesWhenFull = { "Bob... guess what this bottle is for...", "It's a recipe for your dreams!", "let me fill it" };
        public String[] messagesWhenEmpty = { "An empty bottle...", "It looks like something important" };
        
        private bool empty = true;
        private Companion companion;
        private SpriteRenderer dreamBottleRenderer;

        private void Start()
        {
            dreamBottleRenderer = this.GetComponent<SpriteRenderer>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player" && empty)
            {
                OnTouch(other);
            }
        }

        public void OnTouch(Collider2D other)
        {
            if (other.tag == "Player")
            {
                companion = other.GetComponentInChildren<Companion>();
                // We are not filled with energy yet so: funny message, no callback and no freeze
                if (!companion.IsFull())
                {
                    companion.Talk(messagesWhenEmpty);
                    return;
                }

                other.GetComponent<Character>().MovementState.ChangeState(CharacterStates.MovementStates.Idle);
                LevelManager.Instance.FreezeCharacters();
                companion.Talk(messagesWhenFull, this);
            }
        }

        public void CallBackAction()
        {
            if (empty && companion.IsFull())
            {
                companion.DeliverEnergyTo(this.gameObject);
                empty = false;
                dreamBottleRenderer.sprite = fullSprite;
                PlaySound(sound);
                LevelManager.Instance.UnFreezeCharacters();
                LevelCompleteScene.Instance.LevelCompleteTasks();
            }
        }



        private void PlaySound(AudioClip sound)
        {
            if (sound != null)
                SoundManager.Instance.PlaySound(sound, transform.position);
        }
    }
}