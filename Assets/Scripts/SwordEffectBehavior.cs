﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class SwordEffectBehavior : StateMachineBehaviour {

	public GameObject swordEffectPrefab;
	private GameObject swordEffect;
	private Animator swordAnim;
	bool facingRight;
	bool effectFacingRight;
    float effectOffset;
    private bool flip;
    private MMTouchButton touchButtonA;
    private MMTouchButton touchButtonRT;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        #if UNITY_ANDROID
            touchButtonA = GameObject.FindGameObjectWithTag("ButtonA").GetComponent<MMTouchButton>();
            touchButtonRT = GameObject.FindGameObjectWithTag("ButtonRT").GetComponent<MMTouchButton>();
            touchButtonA.DisableButton();
            touchButtonRT.DisableButton();
        #endif
        swordEffect = Instantiate(swordEffectPrefab, Vector3.zero, animator.transform.rotation) as GameObject;

        effectOffset = 0.15f;
        flip = false;

        swordAnim = swordEffect.GetComponent<Animator>();
        swordAnim.SetTrigger("SwordEffect");
        GameObject.Find("UICamera").GetComponent<InputManager>().InputDetectionActive = false;

        //bobPosition = GameObject.Find("NewStrangeBob").GetComponent<Transform>().position;

        //if(swordEffect == null)
        // swordEffect = Instantiate (swordEffectPrefab, bobPosition, animator.transform.rotation) as GameObject;

        //swordEffect.GetComponent<SpriteRenderer>().enabled = true;

        //swordAnim = swordEffect.GetComponent<Animator>();
        //swordAnim.Play("SwordEffect");
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        facingRight = GameObject.Find("NewStrangeBob").GetComponent<Character>().IsFacingRight;
        effectFacingRight = swordEffect.GetComponent<SpriteRenderer>().flipX;


        if(facingRight == effectFacingRight)
        {
            flip = true;
            effectOffset *= -1;
        }

        if (flip)
            Flip();

        //bobPosition = GameObject.Find("NewStrangeBob").GetComponent<Transform>().position;
        //swordEffect.transform.rotation = GameObject.Find("NewStrangeBob").GetComponent<Transform>().rotation;
        swordEffect.transform.position = animator.rootPosition + new Vector3(effectOffset, -0.02f, 0);


        if (!swordEffect.GetComponent<SpriteRenderer>().enabled)
        {
            swordEffect.GetComponent<SpriteRenderer>().enabled = true;
        }


        //facingRight = GameObject.Find("NewStrangeBob").GetComponent<Character>().IsFacingRight;

        //if (facingRight != swordEffectRight) {
        //Flip ();
        //}

        //swordEffect.transform.position = animator.transform.position + new Vector3(0.15f,-0.02f,0);
        //if (swordAnim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.50f) {
        //	swordEffect.SetActive (false);
        //}

        //Por algum motivo sinistro, o ultimo frame da animação fica um tempo extra na tela. Esse código abaixo detecta se a animação já acabou, esconde
        //o gameobject todo e no onstateexit eu destruo ele, pra evitar de ficar lançando exceções em runtime
        //if (swordAnim.GetCurrentAnimatorStateInfo (0).normalizedTime > 1)
        //swordEffect.SetActive(false);
        //if (!swordAnim.GetCurrentAnimatorClipInfoCount()) {
        //	Destroy (swordEffect);
        //}
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    { 
        Destroy (swordEffect);
        GameObject.Find("UICamera").GetComponent<InputManager>().InputDetectionActive = true;
        #if UNITY_ANDROID
            touchButtonA.EnableButton();
            touchButtonRT.EnableButton();
        #endif
        //Debug.Log (swordAnim.GetCurrentAnimatorClipInfoCount (0));
        //swordRend.enabled = false;
        //swordAnim.
        //swordAnim.Play ("swordeffect");
    }

    //OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //facingRight = GameObject.Find("NewStrangeBob").GetComponent<Character>().IsFacingRight;
    //bobPosition = GameObject.Find("NewStrangeBob").GetComponent<Transform>().position;
    //swordEffect.transform.rotation = GameObject.Find("NewStrangeBob").GetComponent<Transform>().rotation;
    //swordEffect.transform.position = bobPosition + new Vector3(0.15f, -0.02f, 0);

    //if (facingRight != swordEffectRight)
    //{
    //Flip();
    //}
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //}

    void Flip(){
        effectFacingRight = !effectFacingRight;
		swordEffect.GetComponent<SpriteRenderer> ().flipX = true;
	}
}
