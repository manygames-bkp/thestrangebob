﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using UnityEngine;

public class EnemyOnDeath : CharacterAbility {

    [System.Serializable]
    public struct Drop
    {
        [Header("The Drop Prefab")]
        public GameObject dropPrefab;
        [Header("Amount of drops that will be instantiated")]
        public int amount;
    }

    [Header("List of things to drop on death")]
    public List<Drop> drops;


    private Explodable _explodable;
    //private ExplosionForce _explosionForce;

    protected override void Start()
    {
        _explodable = GetComponent<Explodable>();
        //_explosionForce = GetComponent<ExplosionForce>();
    }

    protected override void OnDeath()
    {
        base.OnDeath();

        _explodable.explode();
        //_explosionForce.doExplosion(transform.position);

        PlayAbilityStartSfx();

        Invoke("ThrowDrops", 0.5f);
    }

    private void ThrowDrops()
    {
        foreach (Drop drop in drops)
        {
            for (int i = 0; i < drop.amount; i++)
            {
                GameObject droping = Instantiate(drop.dropPrefab, new Vector3(transform.position.x + (-0.03f + (0.01f * i)), transform.position.y + 0.1f), transform.rotation);
                droping.GetComponent<Rigidbody2D>().AddForce(new Vector2(-0.8f + (0.5f * i), Curve(i)));
            }
        }
    }

    private float Curve(int i)
    {
        switch (i)
        {
            case 0:
                return 2.7f;
            case 1:
                return 3.3f;
            case 2:
                return 3.9f;
            case 3:
                return 3f;
            case 4:
                return 2.2f;
            default:
                return 3f;
        }
    }
}
