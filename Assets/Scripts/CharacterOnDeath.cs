﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class CharacterOnDeath : CharacterAbility {

    protected override void OnDeath()
    {
        base.OnDeath();
        PlayAbilityStartSfx();
        GameManager.Instance.LoseLife();
    }
}
