﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.CorgiEngine;
using UnityEngine;

public class AreaOpener : MonoBehaviour
{     
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            LevelManager.Instance.LevelBounds.center = Vector3.zero;
            LevelManager.Instance.LevelBounds.Expand(5.46f);
            //LevelManager.Instance.LevelBounds = new Bounds(Vector3.zero, new Vector3(5.46f, 6f, 0f));
            Camera.main.GetComponent<CameraController>().UpdateLevelBounds();
            GameObject.Find("NewStrangeBob").GetComponent<CharacterLevelBounds>().UpdateLevelBounds();
            Destroy(gameObject);
        }
    }
}
