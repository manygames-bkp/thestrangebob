﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;
namespace MoreMountains.CorgiEngine
{
    public class Companion : MonoBehaviour
    {
        public Sprite emptySprite;
        public Sprite fullSprite;
        /// the font that should be used to display the text
        public Font TextFont;
        /// the size of the font
        public int TextSize = 20;
        /// the color of the text background.
        public Color TextBackgroundColor = Color.black;
        /// the color of the text
        public Color TextColor = Color.white;

        private Transform masterTransform;
        private SpriteRenderer companionRenderer;
        private bool fullEnergy = false;
        private DialogueBox dialogueBox;

        void Start()
        { 
            SetMaster(GameObject.Find("NewStrangeBob").GetComponent<Transform>());
            companionRenderer = this.GetComponent<SpriteRenderer>();
            this.GetComponent<ParticleSystem>().Stop();
            dialogueBox = this.GetComponentInChildren<DialogueBox>();
        }

        void Update()
        {      
            transform.position = masterTransform.position;
            //dialogueBox.transform.position = transform.position;
        }

        private void SetMaster(Transform transform)
        {
            masterTransform = transform;
        }

        public void DeliverEnergyTo(GameObject target)
        {
            // Companion flies to the energy bottle and transfer it's energy
            //GameManager.Instance.SetTimeScale(0f);
            //this.GetComponent<ParticleSystem>().GetComponent<Renderer>().enabled = true
            Vector3.MoveTowards(this.transform.position, target.transform.position, 5f * Time.deltaTime);
            this.GetComponent<ParticleSystem>().Stop();
            fullEnergy = false;
            companionRenderer.sprite = emptySprite;
        }

        public void ReceiveEnergyFrom(GameObject target)
        {
            // Companion flies to the energy bottle and transfer it's energy
            //GameManager.Instance.SetTimeScale(0f);
            //this.GetComponent<ParticleSystem>().GetComponent<Renderer>().enabled = true;
            Vector3.MoveTowards(this.transform.position, target.transform.position, 5f * Time.deltaTime);
            this.GetComponent<ParticleSystem>().Play();
            fullEnergy = true;
            companionRenderer.sprite = fullSprite;
        }

        public bool IsFull()
        {
            return fullEnergy;
        }
        // Talk without callback and no freeze
        public void Talk(String[] messages)
        {
            Talk(messages, null);
        }

        public void Talk(String[] messages, IDialogueOnTouch other)
        {
            dialogueBox.ChangeColor(TextBackgroundColor, TextColor);
            if (TextFont != null)
            {
                dialogueBox.DialogueText.font = TextFont;
            }
            if (TextSize != 0)
            {
                dialogueBox.DialogueText.fontSize = TextSize;
            }

            foreach (Transform child in dialogueBox.transform)
            {
                child.gameObject.SetActive(true);
            }

            StartCoroutine(TalkRoutine(messages, other));
        }

        private IEnumerator TalkRoutine(String[] messages, IDialogueOnTouch other)
        {
            foreach (String message in messages)
            {
                dialogueBox.ChangeText(message);
                dialogueBox.FadeIn(0.3f);
                yield return new WaitForSeconds(3f);
                dialogueBox.FadeOut(0.3f);
            }
            if(other != null)
                other.CallBackAction();
        }
    }
}