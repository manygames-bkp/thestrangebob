﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class LevelCompleteScene : Singleton<LevelCompleteScene>
    {   
        public AudioClip sound;
        public AdManager adManager;

        void Start()
        {
            this.GetComponent<ParticleSystem>().Stop();
        }

        public void LevelCompleteTasks()
        {
            adManager.RequestInterstitial();
            StartCoroutine(LevelCompleteAnimation());
        }

        private IEnumerator LevelCompleteAnimation()
        {
            yield return new WaitForSeconds(1f);
            this.GetComponent<ParticleSystem>().Play();
            //Entender como fazer o som ser religado toda vez que o Load dos saves com MusicOff for dado
            //SoundManager.Instance.MusicOff();
            SoundManager.Instance.PlaySound(sound, transform.position);
            yield return new WaitForSeconds(5f);
            MMEventManager.TriggerEvent(new CorgiEngineEvent(CorgiEngineEventTypes.LevelComplete));
            MMEventManager.TriggerEvent(new MMGameEvent("Save"));
            LevelManager.Instance.SetNextLevel("SecondDream");

            if (adManager.intersticial.IsLoaded())
                adManager.intersticial.Show();
            else
                Debug.Log("Ad Not Ready");
        }
    }
}