﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;

using UnityEngine;

public class SecretRoomDoor : MonoBehaviour {

    public SpriteMask secretRoomMask;
    public bool isDoorToTheRight;

    private Material backup;
    private Material lights;
    private TilemapRenderer outside;
    private enum CollisionDirection { FromLeft, FromRight, ByLeft, ByRight }
    private enum CollisionState { Entering, Leaving }
    private Camera mainCamera;
    private Color originalColor;

    private void Start()
    {
        mainCamera = Camera.main;

        lights = (Material) Resources.Load("Materials/Lights");
        outside = GameObject.Find("CollidableOutsideSecretRoom").GetComponent<TilemapRenderer>();
        backup = outside.material;

        ColorUtility.TryParseHtmlString("#1b1e28", out originalColor);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //mainCamera.cullingMask = ~(1 << LayerMask.NameToLayer("SecretRoom"));
            //mainCamera.cullingMask = ~(1);
            //mainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Platforms"));
            //mainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Default"));
            Vector3 otherPosition = other.transform.position;

            if (otherPosition.x > transform.position.x)
            {
                ChangeSpriteMaskState(true, CollisionDirection.FromRight ,CollisionState.Entering);
            }
            else
            {
                ChangeSpriteMaskState(false, CollisionDirection.FromLeft, CollisionState.Entering);
            }
            
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        { 
            Vector3 otherPosition = other.transform.position;

            if (otherPosition.x > transform.position.x)
            {
                ChangeSpriteMaskState(false, CollisionDirection.ByRight, CollisionState.Leaving);
            }
            else
            {
                ChangeSpriteMaskState(true, CollisionDirection.ByLeft, CollisionState.Leaving);
            }

        }
    }

    void ChangeSpriteMaskState(bool toState, CollisionDirection direction, CollisionState collState)
    {
        if (isDoorToTheRight)
        {
            if (collState.Equals(CollisionState.Entering) && direction.Equals(CollisionDirection.FromLeft) && secretRoomMask.enabled)
                TurnCullingMasksOn();

            if (collState.Equals(CollisionState.Entering) && direction.Equals(CollisionDirection.FromRight) && !secretRoomMask.enabled)
                TurnCullingMasksOff();

            if (collState.Equals(CollisionState.Leaving) && direction.Equals(CollisionDirection.ByLeft) && !secretRoomMask.enabled)
                TurnCullingMasksOff();

            if (collState.Equals(CollisionState.Leaving) && direction.Equals(CollisionDirection.ByRight) && secretRoomMask.enabled)
                TurnCullingMasksOn();

            secretRoomMask.enabled = toState;
        }
        else
        {
            if (collState.Equals(CollisionState.Entering) && !direction.Equals(CollisionDirection.FromLeft) && secretRoomMask.enabled)
                TurnCullingMasksOn();

            if (collState.Equals(CollisionState.Entering) && !direction.Equals(CollisionDirection.FromRight) && !secretRoomMask.enabled)
                TurnCullingMasksOff();

            if (collState.Equals(CollisionState.Leaving) && !direction.Equals(CollisionDirection.ByLeft) && !secretRoomMask.enabled)
                TurnCullingMasksOff();

            if (collState.Equals(CollisionState.Leaving) && !direction.Equals(CollisionDirection.ByRight) && secretRoomMask.enabled)
                TurnCullingMasksOn();

            secretRoomMask.enabled = !toState;
        }
    }

    void TurnCullingMasksOn()
    {
        mainCamera.cullingMask = ~(0);
        mainCamera.backgroundColor = originalColor;
        ChangeMaterials(backup);
    }

    void TurnCullingMasksOff()
    {
        mainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Platforms"));
        mainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Default"));
        mainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Enemies"));
        mainCamera.backgroundColor = Color.black;
        ChangeMaterials(lights);
    }

    void ChangeMaterials(Material mat)
    {
        outside.material = mat;
    } 
}