﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	public Texture2D map;
	public ColorToPrefab[] colorMappings;
	Bounds bounds;
	float baseX = 0;
	float baseY = 0;
	float normalX = 0;
	float normalY = 0;
	float maxX = 0;
	float maxY = 0;

	// Use this for initialization
	void Start () {
		GenerateLevel();
	}

	void GenerateLevel(){
		for(int x = 0; x < map.width; x++){
			for(int y = 0; y < map.height; y++){
				GenerateTile(x,y);
			}
		}
	}

	void GenerateTile(int x, int y){
		normalX = x / 1.5f;
		normalY = y / 3.5f;
		Color pixelColor = map.GetPixel(x, y);

		if (pixelColor.a == 0) {
			//transparent, ignore it
			return;
		}

		foreach(ColorToPrefab colorMapping in colorMappings){
			if(colorMapping.color.Equals(pixelColor)){
				//if(colorMapping.prefab.GetComponent<SpriteRenderer>().bounds.max.x > maxX)
				maxX = colorMapping.prefab.GetComponent<SpriteRenderer>().bounds.max.x;


				float xFloat = (float)x;
				baseX = (float) ((maxX * x) + (0.08f* x));
					
				//baseY += maxY;



				Vector2 positions = new Vector2(baseX, normalY);
				//if (pixelColor == Color.red) {
				//	float redX = x/2.15f;
				//	float redY = y/5.0f;
				//	position = new Vector2(redX, redY);
				//}

				//if (pixelColor == Color.black) {
				//	float blackX = x/2.15f;
				//	float blackY = y/5.0f;
				//	position = new Vector2(blackX, blackY);
				//}
				//Solução para o espaçamento entre os objetos plotados na tela foi divir o valor da distância entre eles por 2.15. Era isso ou mexer no pixel to unit size
				// que eu ainda não domino.

				Instantiate(colorMapping.prefab, positions, Quaternion.identity, transform);
			}
		}
	}

}
