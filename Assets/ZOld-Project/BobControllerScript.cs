﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobControllerScript : MonoBehaviour {

	public float maxSpeed = 3f;
	public bool facingRight = true;
	public Rigidbody2D bob2DBody;
	public Animator anim;

	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.05f;
	public LayerMask whatIsGround;

	float jumpForce = 170f;

	public float move;

	public GameObject swordEffectAnim;
	//public Animator swordAnim;
	//public GameObject swordEffect;

	// Use this for initialization
	void Start () {
		
	}


	void Update(){
		if(grounded && Input.GetKeyDown(KeyCode.Space)){
			anim.SetBool("Ground", false);
			bob2DBody.AddForce(new Vector2(0, jumpForce));
		}

		if(Input.GetKeyDown(KeyCode.F)){
			//anim.SetTrigger("Attack");
			anim.Play("Attack");
			//swordEffectAnim.GetComponent<SpriteRenderer>().enabled = true;
			swordEffectAnim.GetComponent<Animator>().SetTrigger("SwordEffect");
			//Animator swordAnim = swordEffect.GetComponent<Animator>();
			//SpriteRenderer swordRend = swordEffect.GetComponent<SpriteRenderer>();
			//swordRend.enabled = true;
			//swordAnim.Play("swordeffect");
			//swordRend.enabled = false;
			//swordAnim.
			//swordAnim.Play ("swordeffect");
		}
	}

	// Update is called once per frame
	void FixedUpdate () {

		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool("Ground", grounded);

		anim.SetFloat ("vSpeed", bob2DBody.velocity.y);

		move = Input.GetAxis("Horizontal");

		anim.SetFloat ("Speed", Mathf.Abs (move));
		//bob2DBody.AddForce (new Vector2(move * maxSpeed, 0));
		bob2DBody.velocity = new Vector2 (move * maxSpeed, bob2DBody.velocity.y);

		if (move > 0 && !facingRight) {
			Flip ();
		} else if(move < 0 && facingRight){
			Flip ();
		}
	}

	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
