﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class OnTouch : MonoBehaviour {
    protected bool activate;
    protected bool damage;

    [Header("Targets")]
    [Information("This component will make your object cause damage to objects that collide with it. Here you can define what layers will be affected by the damage (for a standard enemy, choose Player), how much damage to give, and how much force should be applied to the object that gets the damage on hit. You can also specify how long the post-hit invincibility should last (in seconds).", MoreMountains.Tools.InformationAttribute.InformationType.Info, false)]
    // the layers that will be affected by this object
    public LayerMask TargetLayerMask;

    /// <summary>
    /// When a collision with the player is triggered, we delegate the implementation to the concrete classes
    /// </summary>
    /// <param name="collider">what's colliding with the object.</param>
    public virtual void OnTriggerStay2D(Collider2D collider)
    {
        Colliding(collider);
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        Colliding(collider);
    }

    protected virtual void Colliding(Collider2D collider)
    { 
        Debug.Log("Colliding on OnTouch");
    }
}
