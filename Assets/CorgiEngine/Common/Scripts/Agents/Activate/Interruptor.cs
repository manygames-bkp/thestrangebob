﻿using System;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class Interruptor : MonoBehaviour
    { 
        [Header("Objects that this Interruptor will activate")]
        public List<GameObject> switchables;

        [Header("Initial Sprite")]
        public Sprite initialState;
        [Header("Switched Sprite")]
        public Sprite switchedState;
        private bool wasSwitched = false;

        public virtual void TurnOnOff()
        {
            wasSwitched = !wasSwitched;
            ChangeSprite();
            foreach (var switchable in switchables)
            {
                switchable.GetComponent<ISwitchable>().OnSwitch();
            }
            MMEventManager.TriggerEvent(new MMActivateOnHitEvent());
        }

        private void ChangeSprite()
        {
            if (wasSwitched)
                GetComponent<SpriteRenderer>().sprite = switchedState;
            else
                GetComponent<SpriteRenderer>().sprite = initialState;
        }
    }
}