﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using UnityEngine;

public class ActivateOnTouch : OnTouch {

    protected override void Colliding(Collider2D collider)
    {
        Debug.Log("Colliding on ActivateOnTouch");

        if (!this.isActiveAndEnabled)
        {
            return;
        }

        // if the object we're colliding with is part of our ignore list, we do nothing and exit
        //if (_ignoredGameObjects.Contains(collider.gameObject))
        //{
            //return;
        //}

        // if what we're colliding with isn't part of the target layers, we do nothing and exit
        if (!MMLayers.LayerInLayerMask(collider.gameObject.layer, TargetLayerMask))
        {
            return;
        }

        /*if (Time.time - _knockbackTimer < InvincibilityDuration)
        {
            return;
        }
        else
        {
            _knockbackTimer = Time.time;
        }*/
    }
}
