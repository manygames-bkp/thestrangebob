﻿using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class CharacterLanding : CharacterAbility
{

    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter("Landing", AnimatorControllerParameterType.Bool);
    }

    public override void UpdateAnimator()
    {
        MMAnimator.UpdateAnimatorBool(_animator, "Landing", (_movement.CurrentState == CharacterStates.MovementStates.Landing), _character._animatorParameters);
    }

    public override void ProcessAbility()
    {
        base.ProcessAbility();
        if (_controller.State.JustGotGrounded)
        {
            _movement.ChangeState(CharacterStates.MovementStates.Landing);
        }

        if ((!_character._animator.GetCurrentAnimatorStateInfo(0).IsName("Landing")) && (_movement.CurrentState == CharacterStates.MovementStates.Landing) && !_controller.State.JustGotGrounded)
            _movement.ChangeState(CharacterStates.MovementStates.Idle);
        //while (_character._animator.GetCurrentAnimatorStateInfo(0).IsName("Landing"))
        //{
        //    if (!(_movement.CurrentState == CharacterStates.MovementStates.Landing))
        //    {
        //        _movement.ChangeState(CharacterStates.MovementStates.Idle);
        //    }
        //}
    }

    public void FinishLanding()
    {
        _movement.ChangeState(CharacterStates.MovementStates.Idle);
    }
}