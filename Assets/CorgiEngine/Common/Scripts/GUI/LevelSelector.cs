﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using System;

namespace MoreMountains.CorgiEngine
{
	/// <summary>
	/// This component allows the definition of a level that can then be accessed and loaded. Used mostly in the level map scene.
	/// </summary>
	public class LevelSelector : MonoBehaviour
	{
		/// the exact name of the target level
	    public string LevelName;

		/// <summary>
		/// Loads the level specified in the inspector
		/// </summary>
	    public virtual void GoToLevel()
	    {
            ResetLives();
	        LevelManager.Instance.GotoLevel(LevelName);
	    }

		/// <summary>
		/// Restarts the current level
		/// </summary>
	    public virtual void RestartLevel()
		{
			// we trigger an unPause event for the GameManager (and potentially other classes)
			MMEventManager.TriggerEvent (new CorgiEngineEvent (CorgiEngineEventTypes.UnPause));

            ResetLives();

            LoadingSceneManager.LoadScene(SceneManager.GetActiveScene().name);
	    }

        private void ResetLives()
        {
            // we reset lives
            GameManager.Instance.MaximumLives = GameManager.Instance.MaximumLives;
            GameManager.Instance.CurrentLives = GameManager.Instance.MaximumLives;
        }
    }
}